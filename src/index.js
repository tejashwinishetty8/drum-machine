import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';


class DrumMachine extends React.Component{
    constructor(props){
        super(props);
        this.state = {
        keys : ["Q","W","E","A","S","D","Z","X","C"],
        audio : ['https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3','https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3','https://s3.amazonaws.com/freecodecamp/drums/Chord_1.mp3','https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3','https://s3.amazonaws.com/freecodecamp/drums/punchy_kick_1.mp3','https://s3.amazonaws.com/freecodecamp/drums/Brk_Snr.mp3','https://s3.amazonaws.com/freecodecamp/drums/Dry_Ohh.mp3','https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3','https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3'],
        chords : ['Chord 1','Chord 2','chord 3','Shaker','Open HH','Closed HH','Punchy Kick','Side Stick','Snare'],
        chord : ''
    }
        
        this.playSound = this.playSound.bind(this);
    }
    playSound(e) {
        console.log(this.state.keys[e.target.id]);
        const sound = document.getElementById(this.state.keys[e.target.id]);
        console.log(sound);
        document.getElementById('display').innerHTML = this.state.chords[e.target.id];
        sound.play();
      }
    render(){
        var array = [];
        for(let i = 0; i < 9; i++){
          var elements =
                                    <button key ={i} id={i} className="drum-pad" onClick={this.playSound}>
            {this.state.keys[i]}                        <audio src={this.state.audio[i]} className='clip' id={this.state.keys[i]}></audio>
                                    </button>
        array.push(elements);
        }
        return(
            <div id="drum-wrap">
                <div id="drum-pad">
                    {array}
                </div>
                <div id="chords-display">
                  <p id="display">No Sound</p>  </div>
            </div>);
    }
}

ReactDOM.render(<DrumMachine />, document.getElementById('drum-machine'));
